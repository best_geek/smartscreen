import requests
import logging
from config import *
import pprint as pp
import datetime

#setup logging
logging.basicConfig(format="[%(filename)s:%(lineno)d] [fun:%(funcName)s] %(asctime)s [%(levelname)s] %(message)s", datefmt="%m/%d/%Y_%I:%M:%S")
logging.root.setLevel(logging.DEBUG)

def get_weather():

    global openweather_api_key
    global openweather_base_url
    global openweather_city_name

    degree_sign = u'\N{DEGREE SIGN}'

    now = datetime.datetime.now()
    current_time = now.strftime("%H:%M:%S")

    max_tries=3
    count=0
    
    while count != max_tries:

        if count == max_tries:
            logging.error(f"Get weather hit max tries of {max_tries}")
            return "Not Available"

        complete_url = f"{openweather_base_url}?q={openweather_city_name}&appid={openweather_api_key}"  
        response = requests.get(complete_url)


        if response.status_code == 200:
            logging.info("Got weather update")

            resp_json=response.json()
            #pp.pprint(resp_json)

            temp_calvin=resp_json["main"]["temp"]
            temp_celc=int(temp_calvin/274.15)
            description=resp_json["weather"][0]["description"]

            return_str_formatted=f"(Updated {current_time})\n\nTemperature: {temp_celc}{degree_sign}c\nConditions: {description}"
            return return_str_formatted

        #increment counter
        count+=1

