import tkinter as tk
from tkinter import *
from PIL import ImageTk, Image
import logging
import pprint as pp
import datetime
from config import * 
from weather import * 
from netcheck import * 
from sysstat import * 
from greeting import *
from padstr import *
import sys


#setup logging
logging.basicConfig(format="[%(filename)s:%(lineno)d] [fun:%(funcName)s] %(asctime)s [%(levelname)s] %(message)s", datefmt="%m/%d/%Y_%I:%M:%S")
logging.root.setLevel(logging.DEBUG)

version="beta 1"

window = tk.Tk()

#setup window
window.title('Smart Screen')
window.attributes("-fullscreen", True)  
#window.geometry("1920x1080")
window.configure(bg='black')

greeting = tk.Label(text="Hello, Tkinter")


#grid values
grid_cols=5
grid_rows=5

#set name
username="Nathan"

#store dict X row value by Y row value
#make sure list types matches number of cols
grid_lookup={}


print(grid_lookup)


def text_from_gridlookup(grid_dict={},col=0,row=0):
    """Will look up by indicies for a text value
    
    Arg: grid_dict = the dictionary grid to lookup
    Arg: col = the column number to lookup
    Arg: row = the row number to lookup

    Returns: '' or actual string from lookup
    """

    #logging.debug(f"lookup col {col} with row {row}")

    col_content=grid_dict.get(col,None)

    if col_content:
        try:
            text=col_content[row]
            return text
        except IndexError:
            logging.warning(f"Request for col {col}, row {row} doesn't exist")
            return ""
    else:
        return ""


def make_grid_dict(grid_dict={},max_col=0,max_row=0):
    """Will populate a default dictionary values
    
    Arg: grid_dict = provide any values to overide
    Arg: max_col = the columns to create
    Arg: max_row = the rows number to lookup

    Returns: dict
    """
    new_dict={}

    #max col+1 as indexed from 0
    for c in range(0,max_col):
        new_dict[c]=[]

        
        for r in range(0,max_row):

            #see if there are default values
            lookup_txt=text_from_gridlookup(grid_dict=grid_dict,col=c,row=r)
            if lookup_txt:
                new_dict[c].append(lookup_txt)
            else:
                new_dict[c].append("")
    
    return new_dict


def update_gridlookup_values(grid_dict={},col=0,row=0,value=""):
    """Will update provided dictionary and return modified version
    
    Arg: grid_dict = the dictionary grid to lookup
    Arg: col = the column number to lookup
    Arg: row = the row number to lookup

    Returns: types dict
    """

    logging.debug(f"updating col {col} with row {row}")

    #have to use try and except as value may be null empty string
    try:
        col_content=grid_dict[col]
    except KeyError:
        logging.warning(f"Update for col {col}, row {row} doesn't exist")
        return grid_dict


    if col_content:
        try:
            grid_dict[col][row]=value
        except IndexError:
            logging.warning(f"Update for col {col}, row {row} doesn't exist")
    
    return grid_dict


def label_text_is_file(label_txt=""):
    """Will inspect label text to see if it contains keyphrase for image
    
    Arg: label_txt = the raw string to validate

    Returns: types bool (true/false)
    """

    if label_txt.startswith("file://") and label_txt.endswith(".png"):
        return True

    return False

def extract_filename_from_text(label_txt=""):
    """Will inspect label text to see if it contains keyphrase for image
    
    Arg: label_txt = the raw string to obtain file from 

    Returns: string
    """

    filename=label_txt.split("file://")[1]
    return filename


grid_lookup=update_gridlookup_values(grid_dict=grid_lookup,col=2,row=4,value="blah blah")

grid_update_count=0
next_weather_update=0
next_network_update=0

def update_grid():
    """
    Will regenerate fields to populate into a grid and re-create the grid

    Uses the global variable 'Window' and 'grid_lookup'
    """

    global version
    global grid_lookup
    global grid_update_count
    global next_weather_update 
    global next_network_update
    
    global global_font_size # from config.py
    global string_padding_len # from config.py
    global weather_update_refresh #from config.py
    global network_update_refresh #from config.py
    global network_ping_hosts #from config.py


    #create lookup grid
    #it will update any set values already
    grid_lookup=make_grid_dict(grid_dict=grid_lookup,max_col=grid_cols,max_row=grid_rows)

    #### update fields

    #add name
    #grid_lookup=update_gridlookup_values(grid_dict=grid_lookup,col=0,row=0,value=f"Hello, {username}")

    #add time & version
    now = datetime.datetime.now()
    current_time = now.strftime("%H:%M:%S")
    grid_lookup=update_gridlookup_values(grid_dict=grid_lookup,col=4,row=0,value=f"Version: {version}\nTime: {current_time} ")

    #add weather
    if next_weather_update == grid_update_count:
        logging.info("Updating weather conditions")
        weather_conditions = get_weather()

        logging.debug(weather_conditions)
        #set bottom right
        grid_lookup=update_gridlookup_values(grid_dict=grid_lookup,col=4,row=-1,value=f"{weather_conditions}")
    
        #set counter for next go 
        next_weather_update = grid_update_count + weather_update_refresh
    #add weather image
    grid_lookup=update_gridlookup_values(grid_dict=grid_lookup,col=3,row=-1,value="file://weather.png")


    #add network check
    if next_network_update == grid_update_count:
        logging.info("Updating network status")
        net_status=netcheck(to_check=network_ping_hosts)
        grid_lookup=update_gridlookup_values(grid_dict=grid_lookup,col=0,row=4,value=f"{net_status}")
        #use grid rows as saying last row

        next_network_update = grid_update_count+ network_update_refresh

    systat_text=get_sysstat()
    grid_lookup=update_gridlookup_values(grid_dict=grid_lookup,col=0,row=0,value=f"{systat_text}")


    #add name
    greeting=findgreeting()
    grid_lookup=update_gridlookup_values(grid_dict=grid_lookup,col=grid_cols-1,row=1,value=f"{greeting}, {username}")


    pp.pp(grid_lookup)


    #create grid
    #create rows
    for r in range(grid_cols):
        window.columnconfigure(r, weight=1, minsize=175)
        window.rowconfigure(r, weight=1, minsize=175)
        #logging.debug(f"Creating column {i}")

        #work out justification        
        if r == 0:
            justify_pos="left"
        elif r == grid_rows-1: #indexed from 0
            justify_pos="right"
        else:
            justify_pos="center"


        #create columns
        for c in range(0, grid_cols):
            #logging.debug(f"Creating row {r} for column {i}")

            frame = tk.Frame(
                master=window,
                relief=tk.RAISED,
                borderwidth=0,
                bg="black"
            )
            frame.grid(row=r, column=c, padx=0, pady=0)

            #work out justification        
            if c == 0:
                justify_pos="left"
            elif c == grid_cols-1: #indexed from 0
                justify_pos="right"
            else:
                justify_pos="center"


            #get text to populate label for
            label_text=text_from_gridlookup(grid_dict=grid_lookup,col=c,row=r)
            if not label_text:
                #label_text=f"column {c}, row {r}, just: {justify_pos}"
                label_text=""


            #branch to see if we render image or text
            if not label_text_is_file(label_text):

                #run through justify split first
                processed_label=""
                label_split=label_text.split("\n")
                for i in range (0,len(label_split)):
                    txt=label_split[i]
                    if txt: 
                        padded=apply_str_padding(input=txt,just=justify_pos,linelen=string_padding_len)
                        print(padded)
                        processed_label+=padded

                        if i != len(label_split):
                            processed_label+="\n"
                label_text=processed_label

                #configure text label label
                label = tk.Label(master=frame, text=f"{label_text}",borderwidth=0,bg="black", anchor="e", justify=justify_pos)
                label.config(font=("TkTextFont", global_font_size),bg="black", borderwidth=0,bd=0,foreground="grey")
                label.pack(padx=0, pady=0, fill="y", expand=1)
            
            #is file
            else:

                filename=extract_filename_from_text(label_text)
                photo = PhotoImage(file=filename)                
                label = Label(master=frame,image=photo, anchor="e")
                label.config(bg="black")
                label.image = photo # keep a reference!
                label.place(x=-100, y=0, relwidth=1, relheight=1)
                label.pack()




    #update number of times drawn
    grid_update_count+=1

    #update the window every minute
    window.after(60000, update_grid)


#draw default first
update_grid()

#keep alive
window.mainloop()