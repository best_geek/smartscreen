import logging
import datetime
import psutil

#setup logging
logging.basicConfig(format="[%(filename)s:%(lineno)d] [fun:%(funcName)s] %(asctime)s [%(levelname)s] %(message)s", datefmt="%m/%d/%Y_%I:%M:%S")
logging.root.setLevel(logging.DEBUG)

def get_sysstat():

    now = datetime.datetime.now()
    current_time = now.strftime("%H:%M:%S")
    
    return_string=f"System Stats:\n(Updated {current_time})\n"
    
    #add cpu
    cpuload=psutil.cpu_percent(4)
    return_string+=f"CPU: {cpuload}%\n"

    #add mem
    memload=psutil.virtual_memory()
    memload=memload[2]
    memload=str(memload)+"%"
    return_string+=f"MEM: {memload}\n"


    #add disk
    diskload=psutil.disk_usage('/')
    diskload=diskload[3]
    diskload=str(diskload)+"%"
    return_string+=f"DSK: {diskload}"


    return return_string


