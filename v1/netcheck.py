import subprocess
import logging
import datetime

#setup logging
logging.basicConfig(format="[%(filename)s:%(lineno)d] [fun:%(funcName)s] %(asctime)s [%(levelname)s] %(message)s", datefmt="%m/%d/%Y_%I:%M:%S")
logging.root.setLevel(logging.DEBUG)

def netcheck (to_check=[]):
    logging.info(f"Requesting network check for {to_check}")

    now = datetime.datetime.now()
    current_time = now.strftime("%H:%M:%S")
    
    
    return_str=f"Network Checker\n(updated: {current_time})"

    #loop through each host
    #takes as loose arguements as possible to cross platform accross linux systems
    for host in to_check:
        ping_response = subprocess.Popen(["ping", "-c1", host], stdout=subprocess.PIPE).stdout.read()

        if " 0.0% packet loss" in str(ping_response):
            host_stat="[OK]"
        else:
            host_stat="[OFFLINE]"

        return_str+="{:>20} {:<8}".format("\n"+host+":",host_stat)


    return return_str

print(netcheck(to_check=["192.168.0.1","192.168.0.3"]))