import math
import logging

#setup logging
logging.basicConfig(format="[%(filename)s:%(lineno)d] [fun:%(funcName)s] %(asctime)s [%(levelname)s] %(message)s", datefmt="%m/%d/%Y_%I:%M:%S")
logging.root.setLevel(logging.DEBUG)

logging.debug("Imported padstr function")

def apply_str_padding(input="",just="",linelen=120):
    """Will update provided dictionary and return modified version
    
    Arg: input = the input string
    Arg: just =  how to justify text (left, right, center)
    Arg: lilnelen = max line length

    Returns: string
    """

    if len(input) >= linelen:
        input = input[0:(linelen-3)]

        #handle last char was a space
        #fixes issues where newlines just have whitespace added on the end
        if input[-1]!=" ":
            input +="..."
        else:
            input+="   "
        return input


    if len(input) < linelen:
        remaining = linelen-len(input)

    if just == "left":
        spaces=" "*remaining
        input+=spaces
        return input

    if just == "right":
        spaces=" "*remaining
        input=spaces+input
        return input

    if just == "center":
        if (remaining % 2) == 0:
            pre_spaces=" " * (math.floor(remaining/2))
            after_spaces=" " * (math.ceil(remaining/2))
        else:
            pre_spaces=" " * (math.floor(remaining/2))
            after_spaces=" " * (math.ceil(remaining/2))

        #add spaces before and after
        input=pre_spaces+input
        input+=after_spaces

        return input

    return input


#print(padstring(input="dsfdsfdsfdsfdsfdsfddfdsfdsfsdfsdfdssdfddasdasdsadasdasdasdsadasdasdasdasdasdassdasdasdasdasdssfsjdaklfjakfdljsklfjklfs",just="",linelen=50))
#print(padstring(input="dsffdsjdaklfjafjklfs",just="left",linelen=50))
#print(padstring(input="dsffdsjdaklfjafjklfs",just="right",linelen=50))
#print(padstring(input="dsffdsjdakfjafjklfs",just="center",linelen=50))