const div_id_time = "clock"
const time_refresh_m = 500


//see if cookie actually exists and module enabled
document.cookie = "__" + div_id_time + "__enabled=true"
div_load_cookie = getCookie("__" + div_id_time + "__enabled")
if (div_load_cookie == "") {
    throw new Error("weather widget not enabled")
}




function build_time_widget(title_txt = "") {
    //creates like for like html
    /*
    <div class="applet" id="time">
        <div class="dragger" id="timedrag">
            <h4 style="display: inline-block;vertical-align: middle;">Time</h4>
        </div>
        <p>10:34</p>
    </div>
    */

    applet = document.createElement('div');
    applet.setAttribute("class", "applet");
    applet.setAttribute("id", div_id_time);

    dragger = document.createElement('div');
    dragger.setAttribute("class", "dragger");
    dragger.setAttribute("id", "timedrag");
    dragger.style.textAlign="right";

 

    title = document.createElement('h4');
    title.style.display = "inline-block";
    title.style.verticalAlign = "inline-block";
    title.innerHTML = title_txt



    clock = document.createElement('p')
    clock.innerHTML = new Date().toLocaleTimeString();
    clock.setAttribute("class", "clock");
    clock.setAttribute("id", "p_clock")



    if (document.getElementById(div_id_time) === null) {

        //build up divs with elements
        //starts inner to outer in HTML

        //title
        dragger.append(title)

        //data
        applet.append(dragger)
        applet.append(clock)


        document.body.appendChild(applet) 
    } else {

        //update fields on screen
        document.getElementById("p_clock").innerHTML = new Date().toLocaleTimeString();
    }

}





build_time_widget(title_txt = "Time")
dragElement(document.getElementById(div_id_time)); //make element draggable
placeElementifCookie(id_lookup = div_id_time)

setInterval(function () {
    build_time_widget(title_txt = "Time")
    dragElement(document.getElementById(div_id_time)); //make element draggable

}, time_refresh_m);




