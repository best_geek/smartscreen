const div_id_netcheck = "netcheck"
const netcheck_refresh_m = 60000  //1min

//configure API
let netcheck_api_endpoint = "/netcheck"
let netcheck_full_api_url = "/api"  + netcheck_api_endpoint
console.log("Using API URL: " + netcheck_full_api_url)





//see if cookie actually exists and module enabled
div_load_cookie = getCookie("__" + div_id_netcheck + "__enabled")
if (div_load_cookie == "") {
    throw new Error("netcheck widget not enabled")
}




function build_netcheck_widget(host_lst = [], title_txt = "") {
    //creates like for like html
    /*

    */


    applet = document.createElement('div');
    applet.setAttribute("class", "applet");
    applet.setAttribute("id", div_id_netcheck);
    applet.style.minWidth = "250px"; // override css as hostnames might be long

    dragger = document.createElement('div');
    dragger.setAttribute("class", "dragger");
    dragger.setAttribute("id", "netcheckdrag");

    img = document.createElement('img');
    img.style.vertical = align = "middle"
    img.style.width = "45px"
    img.src = "img/netcheck.png"


    title = document.createElement('h4');
    title.style.display = "inline-block";
    title.style.verticalAlign = "inline-block";
    title.innerHTML = title_txt

    img.style.width = "45px"
    img.src = "img/netcheck.png"

    last_update = document.createElement('p')
    last_update.innerHTML = "Last updated: " + new Date().toLocaleTimeString();
    last_update.setAttribute("class", "lastupdate");
    last_update.setAttribute("id", "p_netcheck_last_update")


    if (document.getElementById(div_id_netcheck) === null) {

        //build up divs with elements
        //starts inner to outer in HTML

        //title
        dragger.append(img)
        dragger.append(title)

        //data
        applet.append(dragger)
        applet.append(last_update)


        for (var i = 0; i < host_lst.length; i++) {
            var item = host_lst[i];
            //returns tuple from api. [0] is hostname
            // [1] is value

            hostname_txt = String(item[0])
            host_status_txt = String(item[1])


            hostname = document.createElement('p');
            hostname.innerHTML = hostname_txt + "      "
            hostname.setAttribute("id", "p_netcheck_lbl" + hostname_txt)
            hostname.setAttribute("class", "p_left")
            applet.append(hostname)


            hostname_status = document.createElement('p');
            hostname_status.innerHTML = host_status_txt
            hostname_status.setAttribute("id", "p_netcheck_lblval" + hostname_txt)
            if (host_status_txt==="online") {
                hostname_status.style.color = "green"
            } else {
                hostname_status.style.color = "red"
            }
            hostname_status.setAttribute("class", "p_right")
            applet.append(hostname_status)

            //add return 
            applet.append(document.createElement('br'))
        }
        //cpu

        document.body.appendChild(applet)
    } else {

        //update fields on screen
        document.getElementById("p_netcheck_last_update").innerHTML = "Last updated: " + new Date().toLocaleTimeString();

        for (var i = 0; i < host_lst.length; i++) {
            var item = host_lst[i];
            //returns tuple from api. [0] is hostname
            // [1] is value
            hostname_txt = String(item[0])
            host_status_txt = String(item[1])

            hostname = document.getElementById("p_netcheck_lbl" + hostname_txt);
            hostname.innerHTML = hostname_txt + "      "

            hostname_status = document.getElementById("p_netcheck_lblval" + hostname_txt);
            hostname_status.innerHTML = host_status_txt
            if (host_status_txt==="online") {
                hostname_status.style.color = "green"
            } else {
                hostname_status.style.color = "red"
            }

        }
    }

}



async function netcheck_do_get_http(url = "") {

    startSpinner() //shows loading

    var request = new XMLHttpRequest();

    //set how to handle respone
    request.onreadystatechange = function () {

        //get the return json
        if (request.status === 200 && request.readyState === request.DONE) {

            //reponse acttually had data
            if (request.responseText.length > 0) {
                request_json = JSON.parse(request.responseText)
                build_netcheck_widget(host_lst = request_json["hosts"], title_txt = "Network Check")

                // place it in historical location
                dragElement(document.getElementById(div_id_netcheck)); // make element draggable
                placeElementifCookie(id_lookup = div_id_netcheck) // place it in historical location


                return
            }
        }

        if (request.status != 200 && request.readyState === request.DONE) {
            //return default values API would have done
            console.log("API unavailable")
            build_netcheck_widget(host_lst = [], title_txt = "Network Check")


            dragElement(document.getElementById(div_id_netcheck)); // make element draggable
            placeElementifCookie(id_lookup = div_id_netcheck) // place it in historical location

            return

        }
    }

    request.open("GET", url, true);
    request.send(null)
}



//runtime
displayStatusMessage(message_txt="Request network checking ...")

netcheck_do_get_http(url = netcheck_full_api_url)

setInterval(function () {
    console.log("refresh net")
    displayStatusMessage(message_txt="Request network checking ...")
    netcheck_do_get_http(url = netcheck_full_api_url)
}, netcheck_refresh_m);