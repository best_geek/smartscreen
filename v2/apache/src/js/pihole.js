const div_id_pihole = "pihole"
const pihole_refresh_m = 60000 //1min

//configure API
let pihole_api_endpoint = "/pihole"
let pihole_full_api_url = "/api" + pihole_api_endpoint
console.log("Using API URL: " + pihole_full_api_url)





//see if cookie actually exists and module enabled
document.cookie = "__pihole__enabled=true"
div_load_cookie = getCookie("__" + div_id_pihole + "__enabled")
if (div_load_cookie == "") {
    throw new Error("pihole widget not enabled")
}


function build_pihole_widget(pihole_stats = {}, title_txt = "") {
    //creates like for like html
    /*

    */

    applet = document.createElement('div');
    applet.setAttribute("class", "applet");
    applet.setAttribute("id", div_id_pihole);
    applet.style.minWidth = "250px"; // override css as stats might be long


    dragger = document.createElement('div');
    dragger.setAttribute("class", "dragger");
    dragger.setAttribute("id", "piholedrag");

    img = document.createElement('img');
    img.style.vertical = align = "middle"
    img.style.width = "45px"
    img.src = "img/pihole.png"


    title = document.createElement('h4');
    title.style.display = "inline-block";
    title.style.verticalAlign = "inline-block";
    title.innerHTML = title_txt



    last_update = document.createElement('p')
    last_update.innerHTML = "Last updated: " + new Date().toLocaleTimeString();
    last_update.setAttribute("class", "lastupdate");
    last_update.setAttribute("id", "p_pihole_last_update")

    phstatus = document.createElement('p');
    phstatus.innerHTML = "Pihole status"
    phstatus.setAttribute("id", "p_pihole_phstatus")
    phstatus.setAttribute("class", "p_left")

    phstatus_val = document.createElement('p');
    phstatus_val.innerHTML = pihole_stats["status"]
    phstatus_val.setAttribute("id", "p_pihole_phstatus_val")
    phstatus_val.setAttribute("class", "p_right")
    phstatus_val.style.color="red"
    if (pihole_stats["status"]=="enabled"){
        phstatus_val.style.color="green"
    }



    blocked = document.createElement('p');
    blocked.innerHTML = "Blocked"
    blocked.setAttribute("id", "p_pihole_blocked")
    blocked.setAttribute("class", "p_left")


    blocked_val = document.createElement('p');
    blocked_val.innerHTML = pihole_stats["ads_blocked_today"]
    blocked_val.setAttribute("id", "p_pihole_blocked_val")
    blocked_val.setAttribute("class", "p_right")
    blocked_val.style.color = "red"

    uniquedoms = document.createElement('p');
    uniquedoms.innerHTML = "Unique domains"
    uniquedoms.setAttribute("id", "p_pihole_uniquedoms")
    uniquedoms.setAttribute("class", "p_left")


    uniquedoms_val = document.createElement('p');
    uniquedoms_val.innerHTML = pihole_stats["unique_domains"]
    uniquedoms_val.setAttribute("id", "p_pihole_uniquedoms_val")
    uniquedoms_val.setAttribute("class", "p_right")
    uniquedoms_val.style.color = "orange"

    todayq = document.createElement('p');
    todayq.innerHTML = "Queries today"
    todayq.setAttribute("id", "p_pihole_todayq")
    todayq.setAttribute("class", "p_left")


    todayq_val = document.createElement('p');
    todayq_val.innerHTML = pihole_stats["dns_queries_today"]
    todayq_val.setAttribute("id", "p_pihole_todayq_val")
    todayq_val.setAttribute("class", "p_right")
    todayq_val.style.color = "lightblue"



    if (document.getElementById(div_id_pihole) === null) {
        //build up divs with elements
        //starts inner to outer in HTML

        //title
        dragger.append(img)
        dragger.append(title)

        //data
        applet.append(dragger)
        applet.append(last_update)

        //phstatus
        applet.append(phstatus)
        applet.append(phstatus_val)
        applet.append(document.createElement('br'))

        //bloacked
        applet.append(blocked)
        applet.append(blocked_val)
        applet.append(document.createElement('br'))


        //unique doms
        applet.append(uniquedoms)
        applet.append(uniquedoms_val)
        applet.append(document.createElement('br'))

        //unique doms
        applet.append(todayq)
        applet.append(todayq_val)
        applet.append(document.createElement('br'))

        document.body.appendChild(applet)

    } else {

        //update fields on screen
        document.getElementById("p_pihole_last_update").innerHTML = "Last updated: " + new Date().toLocaleTimeString();
        document.getElementById("p_pihole_phstatus_val").innerHTML = pihole_stats["status"]
        document.getElementById("p_pihole_blocked_val").innerHTML = pihole_stats["ads_blocked_today"]
        document.getElementById("p_pihole_uniquedoms_val").innerHTML = pihole_stats["unique_domains"]
        document.getElementById("p_pihole_todayq_val").innerHTML = pihole_stats["dns_queries_today"]

    }

}



async function pihole_do_get_http(url = "") {


    var request = new XMLHttpRequest();

    //set how to handle respone
    request.onreadystatechange = function () {

        //get the return json
        if (request.status === 200 && request.readyState === request.DONE) {

            //reponse acttually had data
            if (request.responseText.length > 0) {
                request_json = JSON.parse(request.responseText)
                build_pihole_widget(pihole_stats=request_json["stats"], title_txt = "Pihole")

                // place it in historical location
                dragElement(document.getElementById(div_id_pihole)); // make element draggable
                placeElementifCookie(id_lookup = div_id_pihole) // place it in historical location


                return
            }
        }

        if (request.status != 200 && request.readyState === request.DONE) {
            //return default values API would have done
            console.log("API unavailable")
            build_pihole_widget(host_lst = [], title_txt = "Pihole (Not Available)")


            dragElement(document.getElementById(div_id_pihole)); // make element draggable
            placeElementifCookie(id_lookup = div_id_pihole) // place it in historical location

            return

        }
    }

    request.open("GET", url, true);
    request.send(null)
}



//runtime
pihole_do_get_http(url = pihole_full_api_url)
displayStatusMessage(message_txt="Request pihole stats ...")


setInterval(function () {
    displayStatusMessage(message_txt="Request pihole stats ...")
    pihole_do_get_http(url = pihole_full_api_url)
}, pihole_refresh_m);