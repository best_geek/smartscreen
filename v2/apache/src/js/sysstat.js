const div_id_sysstat = "sysstat"
const sysstat_refresh_m = 6000000

//configure API
let sysstat_api_endpoint = "/sysstat"
let sysstat_full_api_url = "/api" + sysstat_api_endpoint
console.log("Using API URL: " + sysstat_full_api_url)





//see if cookie actually exists and module enabled
div_load_cookie = getCookie("__" + div_id_sysstat + "__enabled")
if (div_load_cookie == "") {
    throw new Error("syssat widget not enabled")
}


startSpinner()

function build_sysstat_widget(mem_txt = "", dsk_txt = "", cpu_txt = "", title_txt = "") {
    //creates like for like html
    /*

    */

    applet = document.createElement('div');
    applet.setAttribute("class", "applet");
    applet.setAttribute("id", div_id_sysstat);

    dragger = document.createElement('div');
    dragger.setAttribute("class", "dragger");
    dragger.setAttribute("id", "sysstatdrag");

    img = document.createElement('img');
    img.style.vertical = align = "middle"
    img.style.width = "45px"
    img.src = "img/weather.png"


    title = document.createElement('h4');
    title.style.display = "inline-block";
    title.style.verticalAlign = "inline-block";
    title.innerHTML = title_txt

    img.style.width = "45px"
    img.src = "img/sysstat.png"

    last_update = document.createElement('p')
    last_update.innerHTML = "Last updated: " + new Date().toLocaleTimeString();
    last_update.setAttribute("class", "lastupdate");
    last_update.setAttribute("id", "p_sysstat_last_update")


    cpu = document.createElement('p');
    cpu.innerHTML = "CPU: "
    cpu.setAttribute("id", "p_sysstat_cpu")
    cpu.setAttribute("class", "p_left")


    cpu_val = document.createElement('p');
    cpu_val.innerHTML = cpu_txt + "%";
    cpu_val.setAttribute("id", "p_sysstat_cpu_val")
    cpu_val.setAttribute("class", "p_right")


    mem = document.createElement('p');
    mem.innerHTML = "Mem: "
    mem.setAttribute("id", "p_sysstat_mem")
    mem.setAttribute("class", "p_left")


    mem_val = document.createElement('p');
    mem_val.innerHTML = mem_txt + "%";
    mem_val.setAttribute("id", "p_sysstat_mem_val")
    mem_val.setAttribute("class", "p_right")


    dsk = document.createElement('p');
    dsk.innerHTML = "Disk: "
    dsk.setAttribute("id", "p_sysstat_dsk")
    dsk.setAttribute("class", "p_left")


    dsk_val = document.createElement('p');
    dsk_val.innerHTML = dsk_txt + "%";
    dsk_val.setAttribute("id", "p_sysstat_dsk_val")
    dsk_val.setAttribute("class", "p_right")


    if (document.getElementById(div_id_sysstat) === null) {

        //build up divs with elements
        //starts inner to outer in HTML

        //title
        dragger.append(img)
        dragger.append(title)

        //data
        applet.append(dragger)
        applet.append(last_update)

        //cpu
        applet.append(cpu)
        applet.append(cpu_val)
        applet.append(document.createElement('br'))

        //mem
        applet.append(mem)
        applet.append(mem_val)
        applet.append(document.createElement('br'))


        //disk
        applet.append(dsk)
        applet.append(dsk_val)
        applet.append(document.createElement('br'))

        document.body.appendChild(applet)
    } else {

        //update fields on screen
        document.getElementById("p_sysstat_last_update").innerHTML = "Last updated: " + new Date().toLocaleTimeString();
        document.getElementById("p_sysstat_cpu_val").innerHTML = cpu_txt + "%"
        document.getElementById("p_sysstat_mem_val").innerHTML = mem_txt + "%"
        document.getElementById("p_sysstat_dsk_val").innerHTML = dsk_txt + "%"
    }

    //stopSpinner()

}



async function sysstat_do_get_http(url = "") {

    var request = new XMLHttpRequest();

    //set how to handle respone
    request.onreadystatechange = function () {

        //get the return json
        if (request.status === 200 && request.readyState === request.DONE) {

            //reponse acttually had data
            if (request.responseText.length > 0) {
                request_json = JSON.parse(request.responseText)
                build_sysstat_widget(mem_txt = request_json["mem"], dsk_txt = request_json["disk_util"], cpu_txt = request_json["cpu"], title_txt = "System stats:")

                // place it in historical location
                dragElement(document.getElementById(div_id_sysstat)); // make element draggable
                placeElementifCookie(id_lookup = div_id_sysstat) // place it in historical location


                return
            }
        }

        if (request.status != 200 && request.readyState === request.DONE) {
            //return default values API would have done
            console.log("API unavailable")
            build_sysstat_widget(mem_txt = "Not available", dsk_txt = "Not available", cpu_txt = "Not available", title_txt = "System stats:")


            dragElement(document.getElementById(div_id_sysstat)); // make element draggable
            placeElementifCookie(id_lookup = div_id_sysstat) // place it in historical location

            return

        }
    }

    request.open("GET", url, true);
    request.send(null)
}



//runtime
displayStatusMessage(message_txt="Request system stats ...")
sysstat_do_get_http(url = sysstat_full_api_url)


setInterval(function () {
    displayStatusMessage(message_txt="Request system stats ...")
    sysstat_do_get_http(url = sysstat_full_api_url)
}, sysstat_refresh_m);