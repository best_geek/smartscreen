function getCookie(cname) {
    // https://www.w3schools.com/js/js_cookies.asp
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

//see if we have a saved location for widget
function placeElementifCookie(id_lookup) {
    //place the element position if the cookie exists
    pos_cookie = "__" + id_lookup + "__pos"
    cookie_data = getCookie(pos_cookie)

    //cookie data stores position command seperated
    if (cookie_data != null) {
        let coords = String(cookie_data).split(",")
        let elem = document.getElementById(id_lookup)

        elem.style.top = coords[0]
        elem.style.left = coords[1]

    }
}

function displayMessage(message_txt = "") {
    var elem = document.getElementById("messagebox")
    elem.style.display = "block"
    document.getElementById("messagebox").style.animation = "fadeInAnimation 2s";

    document.getElementById("messagetext").innerHTML = message_txt

    setInterval(function () {
        elem.style.display = "none"
    }, 5000);
}

function makeProgressBar(maxpx = 100, unique_id = "blah") {
    var container = document.createElement("div")
    container.style.padding = "0px"
    container.style.margin = "0px"

    var per_width = maxpx / 100

    for (count = 0; count < 101; count++) {
        var block = document.createElement("div")


        //give a unique count
        block.setAttribute("id", unique_id + "_perc_" + count)

        block.setAttribute("class", "perc_block")
        block.style.backgroundColor = "whitesmoke"

        //block represents 1% of total progress
        block.style.width = per_width + "px"

        //container.append(block)
        document.getElementById(unique_id).append(block)
    }

    return container
}

function displayStatusMessage(message_txt = "") {
    var display_time_ms = 9000
    var counter_interval = display_time_ms / 100


    var id = (Math.random() + 1).toString(36).substring(7);

    var sdiv = document.createElement('div');
    sdiv.setAttribute("class", "statusbox");
    sdiv.setAttribute("id", id);

    var stext = document.createElement('p')
    stext.setAttribute("class", "statusbox")
    stext.innerHTML = message_txt


    var htmlarea = document.getElementById("statusnotifications")
    htmlarea.appendChild(sdiv)

    //match max px with width in css class statusbox
    var progdiv = document.createElement('div');



    //add to div bottom up 
    sdiv.append(stext)

    progress_bar = makeProgressBar(maxpx = 200, unique_id = id)

    document.getElementById(id).style.animation = "fadeInAnimation 2s";

    var counter = 101

    var countdown = setInterval(function () {
        counter = counter - 1
        color_elem_grey(elemid = id + "_perc_" + counter)

        if (counter == 1) {
            //remove element now we're done
            document.getElementById(id).remove()

            clearInterval(countdown)
        }

    }, counter_interval)



}


//see if we have a saved location for widget
function placeElementifCookie(id_lookup) {
    //place the element position if the cookie exists
    pos_cookie = "__" + id_lookup + "__pos"
    cookie_data = getCookie(pos_cookie)

    //cookie data stores position command seperated
    if (cookie_data != null) {
        let coords = String(cookie_data).split(",")
        let elem = document.getElementById(id_lookup)

        elem.style.top = coords[0]
        elem.style.left = coords[1]

    }
}
//placeElementifCookie(id_lookup = div_id)

function dragElement(elmnt) {
    var pos1 = 0,
        pos2 = 0,
        pos3 = 0,
        pos4 = 0;
    if (document.getElementById(elmnt.id + "header")) {
        /* if present, the header is where you move the DIV from:*/
        document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
    } else {
        /* otherwise, move the DIV from anywhere inside the DIV:*/
        elmnt.onmousedown = dragMouseDown;
    }

    function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        // get the mouse cursor position at startup:
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        // call a function whenever the cursor moves:
        document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        // calculate the new cursor position:
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        // set the element's new position:
        elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
        elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";

    }

    function closeDragElement() {
        /* stop moving when mouse button is released:*/
        document.onmouseup = null;
        document.onmousemove = null;

    }
}



function saveAllDivPos() {
    //saves all divs with a class of applet
    //writes their location to a cookie which can be saved
    var divs = document.getElementsByTagName("DIV");

    for (var i = 0; i < divs.length; i++) {
        var div = divs[i];

        if (div.className == "applet") {
            id = div.id
            div_elem = document.getElementById(id)
            document.cookie = "__" + id + "__pos=" + div_elem.style.top + "," + div_elem.style.left;
        }

    }

    displayMessage(message_txt = "Saved!")
}

function numAppletsOnScreen() {
    //saves all divs with a class of applet
    //writes their location to a cookie which can be saved
    var divs = document.getElementsByTagName("DIV");

    var return_num = 0

    for (var i = 0; i < divs.length; i++) {
        var div = divs[i];

        if (div.className == "applet") {
            return_num++
        }

    }

    return_num
}

function startSpinner() {
    spinner = document.getElementById("loader")
    spinner.style.display = "block"
}


function stopSpinner() {
    spinner = document.getElementById("loader")
    spinner.style.display = "none"
}

function color_elem_grey(elemid) {
    var elem = document.getElementById(elemid)

    if (elem !== null) {
        elem.style.backgroundColor = "grey"
    }

}