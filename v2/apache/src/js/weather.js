const div_id_weather = "weather"
const weather_refresh_m = 60000*5

//configure API
let weather_api_endpoint = "/weather"
let weather_full_api_url = "/api" + weather_api_endpoint
console.log("Using API URL: " + weather_full_api_url)





//see if cookie actually exists and module enabled
//document.cookie = "__" + div_id_weather + "__enabled=true"
div_load_cookie = getCookie("__" + div_id_weather + "__enabled")
if (div_load_cookie == "") {
    throw new Error("weather widget not enabled")
}




function build_weather_widget(cond = "", temp = "", title_txt = "") {
    //creates like for like html
    /*
    <div class="applet" id="weather">
        <div class="dragger" id="weatherdrag">
            <img src="img/weather.png" id="weatherimg" style="vertical-align:middle;width: 45px;">
            <h4 style="display: inline-block;vertical-align: middle;">Weather</h4>
        </div>
        <p>Move</p>
        <p>this</p>
        <p>DIV</p>
    </div>
    */

    applet = document.createElement('div');
    applet.setAttribute("class", "applet");
    applet.setAttribute("id", div_id_weather);

    dragger = document.createElement('div');
    dragger.setAttribute("class", "dragger");
    dragger.setAttribute("id", "weatherdrag");

    img = document.createElement('img');
    img.style.vertical = align = "middle"
    img.style.width = "45px"
    img.src = "img/weather.png"


    title = document.createElement('h4');
    title.style.display = "inline-block";
    title.style.verticalAlign = "inline-block";
    title.innerHTML = title_txt

    img.style.width = "45px"
    img.src = "img/weather.png"

    last_update = document.createElement('p')
    last_update.innerHTML = "Last updated: " + new Date().toLocaleTimeString();
    last_update.setAttribute("class", "lastupdate");
    last_update.setAttribute("id", "p_weather_last_update")



    conidtion = document.createElement('p');
    conidtion.innerHTML = "Conditions: " + cond
    conidtion.setAttribute("id", "p_weather_conditions")


    temperature = document.createElement('p');
    temperature.innerHTML = "Temperature: " + temp + "c";
    temperature.setAttribute("id", "p_weather_temperature")


    if (document.getElementById(div_id_weather) === null) {

        //build up divs with elements
        //starts inner to outer in HTML

        //title
        dragger.append(img)
        dragger.append(title)

        //data
        applet.append(dragger)
        applet.append(last_update)
        applet.append(conidtion)
        applet.append(temperature)


        document.body.appendChild(applet)
    } else {

        //update fields on screen
        document.getElementById("p_weather_last_update").innerHTML = "Last updated: " + new Date().toLocaleTimeString();
        document.getElementById("p_weather_conditions").innerHTML = "Conditions: " + cond
        document.getElementById("p_weather_temperature").innerHTML = "Temperature: " + temp + "c";
    }

}



async function weather_do_get_http(url = "") {

    var request = new XMLHttpRequest();

    //set how to handle respone
    request.onreadystatechange = function () {

        //get the return json
        if (request.status === 200 && request.readyState === request.DONE) {

            //reponse acttually had data
            if (request.responseText.length > 0) {
                request_json = JSON.parse(request.responseText)
                build_weather_widget(cond = request_json["conditions"],
                    temperature = request_json["temp"], title_txt = "Weather")

                 // place it in historical location
                 dragElement(document.getElementById(div_id_weather)); // make element draggable
                 placeElementifCookie(id_lookup = div_id_weather) // place it in historical location


                return
            }
        }

        if (request.status != 200 && request.readyState === request.DONE) {
            //return default values API would have done
            console.log("API unavailable")
            build_weather_widget(cond = "not available",
                temperature = "not available", title_txt = "Weather")

            dragElement(document.getElementById(div_id_weather)); // make element draggable
            placeElementifCookie(id_lookup = div_id_weather) // place it in historical location

            return

        }
    }

    request.open("GET", url, true);
    request.send(null)
}



//runtime
displayStatusMessage(message_txt="Request weather ...")
weather_do_get_http(url = weather_full_api_url)

setInterval(function () {
    displayStatusMessage(message_txt="Request weather ...")
    weather_do_get_http(url = weather_full_api_url)
}, weather_refresh_m);