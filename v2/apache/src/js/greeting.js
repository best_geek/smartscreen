const div_id_greeting = "greeting"
const greeting_refresh_m = 500000000

//configure API
let greeting_api_endpoint = "/greeting"
let greeting_full_api_url = "/api" + greeting_api_endpoint
console.log("Using API URL: " + greeting_full_api_url)





//see if cookie actually exists and module enabled
div_load_cookie = getCookie("__" + div_id_greeting + "__enabled")
if (div_load_cookie == "") {
    throw new Error("greeting widget not enabled")
}




function build_greeting_widget(greeting_txt = "", title_txt = "") {
    //creates like for like html
    /*
    <div class="applet" id="weather">
        <div class="dragger" id="weatherdrag">
            <img src="img/weather.png" id="weatherimg" style="vertical-align:middle;width: 45px;">
            <h4 style="display: inline-block;vertical-align: middle;">Weather</h4>
        </div>
        <p>Move</p>
        <p>this</p>
        <p>DIV</p>
    </div>
    */

    //add username
    username=getCookie("__username")
    greeting_txt=greeting_txt + ", "+username
    greeting_txt=greeting_txt.replace(/["]+/g, '')

    applet = document.createElement('div');
    applet.setAttribute("class", "applet");
    applet.setAttribute("id", div_id_greeting);

    dragger = document.createElement('div');
    dragger.setAttribute("class", "dragger");
    dragger.setAttribute("id", "greetingdrag");



    title = document.createElement('h4');
    title.style.display = "inline-block";
    title.style.verticalAlign = "inline-block";
    title.innerHTML = "<br>"


    greeting = document.createElement('p');
    greeting.innerHTML = greeting_txt
    greeting.setAttribute("id", "p_greeting")
    greeting.setAttribute("class", "greeting");


    if (document.getElementById(div_id_greeting) === null) {

        //build up divs with elements
        //starts inner to outer in HTML

        //title
        dragger.append(title)

        //data
        applet.append(greeting)
        document.body.appendChild(applet)

    } else {

        //update fields on screen
        document.getElementById("p_greeting").innerHTML = greeting_txt
    }

}



async function greeting_do_get_http(url = "") {

    var request = new XMLHttpRequest();

    //set how to handle respone
    request.onreadystatechange = function () {

        //get the return json
        if (request.status === 200 && request.readyState === request.DONE) {

            //reponse acttually had data
            if (request.responseText.length > 0) {
                build_greeting_widget(greeting=request.responseText)
                dragElement(document.getElementById(div_id_greeting)); // make element draggable
                placeElementifCookie(id_lookup = div_id_greeting) // place it in historical location
                // place it in historical location


                return
            }
        }

        if (request.status != 200 && request.readyState === request.DONE) {
            //return default values API would have done
            console.log("API unavailable")
            build_greeting_widget(greeting="Seems to be a problem getting you a fresh new greeting")

            dragElement(document.getElementById(div_id_greeting)); // make element draggable
            placeElementifCookie(id_lookup = div_id_greeting) // place it in historical location

            return

        }
    }

    request.open("GET", url, true);
    request.send(null)
}



//runtime
greeting_do_get_http(url = greeting_full_api_url)

setInterval(function () {
    greeting_do_get_http(url = greeting_full_api_url)
}, greeting_refresh_m);