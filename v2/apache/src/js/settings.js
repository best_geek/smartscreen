//configure API
let get_settings_api_endpoint = "/settings/get"
let get_settings_full_url = "/api"+ get_settings_api_endpoint

let post_settings_api_endpoint = "/settings/post"
let post_settings_full_url = "/api" + post_settings_api_endpoint

let config_post = {}


function delete_cookie(cookiename = "") {
    document.cookie = cookiename + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT"
}

function getCookie(cname) {
    // https://www.w3schools.com/js/js_cookies.asp
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function displayMessage(message_txt = "", color = "orange") {
    var elem = document.getElementById("messagebox")
    elem.style.display = "block"
    elem.style.borderColor = color
    document.getElementById("messagebox").style.animation = "fadeInAnimation 2s";

    document.getElementById("messagetext").innerHTML = message_txt

    setInterval(function () {
        elem.style.display = "none"
    }, 5000);
}

function show_arrow_when_overflow_set() {
    downarr = document.getElementById("downarr")
    setting_div = document.getElementById("settingsdiv")


    if ((setting_div.scrollHeight) > (setting_div.clientHeight)) {
        console.log("element overflowed")
        setting_div.style.display = "block"
    } else {
        console.log("All settings in view")
        downarr.style.display = "none"
        

    }


}

function validate_weather() {
    //returns false if failed
    let checkedweather = document.getElementById("weatherenable")
    let weather_api_key = document.getElementById("weatheropenweatherAPIkey")
    let weather_city = document.getElementById("weatheropenweathercity")

    if (checkedweather.checked) {
        document.cookie = "__weather__enabled=true"
    } else {
        delete_cookie(cookiename = "__weather__enabled")
    }

    //check fields
    if (weather_api_key.value.length == 0 && checkedweather.checked) {
        displayMessage(message_txt = "Please enter an API key weather", color = "red")
        return false
    }

    if (weather_city.value.length == 0 && checkedweather.checked) {
        displayMessage(message_txt = "Please enter a city for weather", color = "red")
        return false
    }

    //continue now we've satisfied checks
    config_post["weather"] = {}
    config_post["weather"]["openweather_api_key"] = weather_api_key.value
    config_post["weather"]["openweather_city_name"] = weather_city.value

}


function validate_netcheck() {
    //returns false if failed
    let checkednetcheck = document.getElementById("netcheckdisable")
    let hosts_to_check = document.getElementById("netcheckhosts")

    if (checkednetcheck.checked) {
        document.cookie = "__netcheck__enabled=true"
    } else {
        delete_cookie(cookiename = "__netcheck__enabled")
    }

    //check fields
    if (hosts_to_check.value.length == 0 && checkednetcheck.checked) {
        displayMessage(message_txt = "Please enter hosts to check for net-checking", color = "red")
        return false
    }

    //continue now we've satisfied checks
    config_post["netcheck"] = {}
    config_post["netcheck"]["hosts_to_check"] = hosts_to_check.value.split(",")
}

function validate_greeting() {
    //returns false if failed
    let grettingcheck = document.getElementById("grettingdisable")
    let customname = document.getElementById("customname")

    if (grettingcheck.checked) {
        document.cookie = "__greeting__enabled=true"
    } else {
        delete_cookie(cookiename = "__greeting__enabled")
    }

    //check fields
    if (customname.value.length == 0 && grettingcheck.checked) {
        displayMessage(message_txt = "Please enter your custom name", color = "red")
        return false
    } else {
        document.cookie = "__username=" + customname.value
    }
}

function validate_systemstats() {
    //returns false if failed
    let systatcheck = document.getElementById("systemstatsdisable")

    if (systatcheck.checked) {
        document.cookie = "__sysstat__enabled=true"
    } else {
        delete_cookie(cookiename = "__sysstat__enabled")
    }


}

function validate_pihole() {
    //returns false if failed
    let piholechecked = document.getElementById("piholedisable")
    let piholehost = document.getElementById("piholehost")

    if (piholechecked.checked) {
        document.cookie = "__pihole__enabled=true"
    } else {
        delete_cookie(cookiename = "__pihole__enabled")
    }

    //check fields
    if (piholehost.value.length == 0 && piholechecked.checked) {
        displayMessage(message_txt = "Please enter host for Pihole", color = "red")
        return false
    }

    //continue now we've satisfied checks
    config_post["pihole"] = {}
    config_post["pihole"]["piholehost"] = piholehost.value
}



function populate_values_on_screen(settings_response = {}) {

    //do weather
    fill_textbox_from_dict_val(val = "weather.openweather_api_key",
        elem = "weatheropenweatherAPIkey", data = settings_response)

    fill_textbox_from_dict_val(val = "weather.openweather_city_name",
        elem = "weatheropenweathercity", data = settings_response)

    checkbox_if_cookie(cookiename = "__weather__enabled", elem = "weatherenable")

    //TODO: put network checkig hosts
    checkbox_if_cookie(cookiename = "__netcheck__enabled", elem = "netcheckdisable")
    fill_textbox_from_dict_val(val = "netcheck.hosts_to_check",
        elem = "netcheckhosts", data = settings_response)

    checkbox_if_cookie(cookiename = "__greeting__enabled", elem = "grettingdisable")
    document.getElementById("customname").value = getCookie(cname = "__username")

    checkbox_if_cookie(cookiename = "__sysstat__enabled", elem = "systemstatsdisable")

    //pihhole
    checkbox_if_cookie(cookiename = "__pihole__enabled", elem = "piholedisable")
    fill_textbox_from_dict_val(val = "pihole.piholehost",
    elem = "piholehost", data = settings_response)

    




}

function fill_textbox_from_dict_val(val = "", elem = "none", data = {}) {
    //searches for a key value and will populate if present

    //get nested values from a dict of they exist. Must interate thru as if we ask
    //for a tested value like weather.city if weather doesn't exit JS will crash
    //this allows us to do check safely
    console.log("[debug]: enum keys from settings response" + val)
    keys = val.split(".")
    obj = data
    for (var i = 0; i < keys.length; i++) {
        key = keys[i]
        if (!key || !obj.hasOwnProperty(key)) {
            console.warn("Requested key from dict for textbox. " + key + " doesn't exist")
            return false;
        }
        obj = obj[key];
    }

    document.getElementById(elem).value = obj

    return
}

function checkbox_if_cookie(cookiename = "", elem = "") {
    //tested and was string true, not bool true 
    if (getCookie(cname = cookiename) === "true") {
        chkbox = document.getElementById(elem)
        chkbox.checked = true
    }
    return
}

async function settings_get_current(url = "") {

    console.log("Getting current settings via HTTP request")

    var request = new XMLHttpRequest();

    //set how to handle respone
    request.onreadystatechange = function () {

        //get the return json
        if (request.status === 200 && request.readyState === request.DONE) {

            //reponse acttually had data
            if (request.responseText.length > 0) {
                request_json = JSON.parse(request.responseText)
                populate_values_on_screen(settings_response = request_json)

                return
            }
        }

        if (request.status != 200 && request.readyState === request.DONE) {
            //return default values API would have done
            displayMessage(message_txt = "Could not get exisiting settings. Please refresh", color = "red")
            return

        }
    }

    request.open("GET", url);
    request.send(null)
}


function settings_update(url = "") {

    console.log("Updating settings via HTTP request")

    var request = new XMLHttpRequest();
    request.open("GET", url);

    request.setRequestHeader("Accept", "application/json");
    request.setRequestHeader("Content-Type", "application/json");


    request.setRequestHeader("updated_settings", JSON.stringify(config_post));

    console.log(config_post)

    //set how to handle respone
    request.onreadystatechange = function () {

        //get the return json
        if (request.status === 200 && request.readyState === request.DONE) {

            //reponse acttually had data
            if (request.responseText.length > 0) {
                displayMessage(message_txt = "Config saved!", color = "green")
                console.log(JSON.parse(request.responseText))

                return
            }
        }

        if (request.status != 200 && request.readyState === request.DONE) {
            //return default values API would have done
            displayMessage(message_txt = "Could not get update settings. Reason: " + request.status, color = "red")
            return

        }
    }

    request.send(null)
}

function update_config() {
    if (validate_weather() === false) {
        return
    }

    if (validate_netcheck() === false) {
        return
    }

    if (validate_greeting() === false) {
        return
    }

    if (validate_systemstats() === false) {
        return
    }

    if (validate_pihole() === false) {
        return
    }

    settings_update(url = post_settings_full_url)
    //uses global var which contains data

}

settings_get_current(url = get_settings_full_url)

setInterval(show_arrow_when_overflow_set(), 6000);

