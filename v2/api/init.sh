#!/bin/bash
echo "[START] Current path: ${PATH}"
echo "[START] Current user: $(whoami)"
echo "[START] Python ver: $(python3 --version)"


#python3 /statusscreen/api_server.py
cd /statusscreen
echo "[START] Start Gunicorn"
echo ""
gunicorn --bind 0.0.0.0:5001 api_server:app
