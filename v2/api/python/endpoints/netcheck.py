import subprocess
import logging
import datetime
from endpoints.config.config import *


#setup logging
logging.basicConfig(format="[%(filename)s:%(lineno)d] [fun:%(funcName)s] %(asctime)s [%(levelname)s] %(message)s", datefmt="%m/%d/%Y_%I:%M:%S")
logging.root.setLevel(logging.DEBUG)

def netcheck (to_check=[]):
    
    now = datetime.datetime.now()
    current_time = now.strftime("%H:%M:%S")
    

    global config_fname

    current_conf=open_config(fname=config_fname)
    to_check=current_conf.get("netcheck",{}).get("hosts_to_check",None)

    if not to_check:
        return_dict={}
        return_dict["last_updated"]=current_time
        return_dict["hosts"]=[["Unconfigured","Unconfigured"]]
        return return_dict



    logging.info(f"Requesting network check for {to_check}")


    
    return_dict={}
    return_dict["last_updated"]=current_time
    return_dict["hosts"]=[]

    #loop through each host
    #takes as loose arguements as possible to cross platform accross linux systems
    for host in to_check:

        host=host.replace(" ","") #incase user inputted with spaces due to csv formatting

        ping_response = subprocess.Popen(["/bin/ping", "-c1","-W1", host], stdout=subprocess.PIPE).stdout.read()


        if " 0% packet loss" in str(ping_response):
            toadd=(host,"online")
        else:
            toadd=(host,"error")
            logging.debug(str(ping_response))

        return_dict["hosts"].append(toadd)

    return return_dict


#so we can run as a module
if __name__ == "__main__":
    print(netcheck(to_check=network_ping_hosts))