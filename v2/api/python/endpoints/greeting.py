import datetime
import random

def greeting():
    
    greetings=["Hey there",
    "Wow",
    "Looking good",
    "Hiya",
    "Bonjour",
    "Holla",
    "Welcome",
    "Welcome to the party",
    "Looking fly",
    "Ello",
    "Howdy",
    "Sup",
    "Yo!",
    "Waddup",
    "How you doin"]

    now = datetime.datetime.now()
    current_time = now.strftime("%H")
    current_time=int(current_time)

    print(current_time)
    
    if current_time > 20:
        greetings.append("Good evening")

    if current_time > 21: 
        greetings.append("Good night")

    elif current_time > 0 and current_time < 6:
        greetings.append("Good night")
    
    elif current_time > 6 and current_time < 12:
        greetings.append("Good morning")

    elif current_time > 12 and current_time < 16:
        greetings.append("Good afternoon")

    elif current_time > 16 and  current_time < 21:
        greetings.append("Good evening")

    rand_index=random.randint(0,(len(greetings)-1))
    return greetings[rand_index]
