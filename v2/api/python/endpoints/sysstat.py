import logging
import datetime
import psutil
from endpoints.config.config import *


#setup logging
logging.basicConfig(format="[%(filename)s:%(lineno)d] [fun:%(funcName)s] %(asctime)s [%(levelname)s] %(message)s", datefmt="%m/%d/%Y_%I:%M:%S")
logging.root.setLevel(logging.INFO)

def get_sysstat():

    logging.info("request systats")

    now = datetime.datetime.now()
    current_time = now.strftime("%H:%M:%S")
    
    return_dict={}
    return_dict["last_updated"]=current_time


    
    #add cpu
    cpuload=psutil.cpu_percent(4)
    return_dict["cpu"]=cpuload

    #add mem
    memload=psutil.virtual_memory()
    memload=memload[2]
    return_dict["mem"]=memload


    #add disk
    diskload=psutil.disk_usage('/')
    diskload=diskload[3]
    return_dict["disk_util"]=diskload


    return return_dict


