import requests
import logging
from endpoints.config.config import *
import datetime

#setup logging
logging.basicConfig(format="[%(filename)s:%(lineno)d] [fun:%(funcName)s] %(asctime)s [%(levelname)s] %(message)s", datefmt="%m/%d/%Y_%I:%M:%S")
logging.root.setLevel(logging.INFO)

def weather():

    global config_fname
    global openweather_base_url
    global openweather_city_name

    #open and get current config
    current_conf=open_config(fname=config_fname)
    openweather_api_key=current_conf.get("weather",{}).get("openweather_api_key",None)
    openweather_city_name=current_conf.get("weather",{}).get("openweather_city_name",None)
    openweather_base_url="http://api.openweathermap.org/data/2.5/weather"


    now = datetime.datetime.now()
    current_time = now.strftime("%H:%M:%S")

    if not openweather_city_name or not openweather_api_key:
        return_dict={}
        return_dict["conditions"]="Unconfigured" #set default
        return_dict["temp"]="Unconfigured" 
        return_dict["last_updated"]=current_time  

        return return_dict     


    return_dict={}
    return_dict["conditions"]="Not Available" #set default
    return_dict["temp"]="Not Available" 
    return_dict["last_updated"]=current_time
    

    complete_url = f"{openweather_base_url}?q={openweather_city_name}&appid={openweather_api_key}"  
    response = requests.get(complete_url)


    if response.status_code == 200:
        logging.info("Got weather update")

        resp_json=response.json()
        #pp.pprint(resp_json)

        temp_calvin=resp_json["main"]["temp"]
        temp_celc=int(temp_calvin/274.15)
        description=resp_json["weather"][0]["description"]

        return_dict["conditions"]=description
        return_dict["temp"]=temp_celc

        return return_dict

   
    #return default
    return return_dict
