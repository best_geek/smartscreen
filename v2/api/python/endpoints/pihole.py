import requests
import logging
from endpoints.config.config import *
import datetime

#setup logging
logging.basicConfig(format="[%(filename)s:%(lineno)d] [fun:%(funcName)s] %(asctime)s [%(levelname)s] %(message)s", datefmt="%m/%d/%Y_%I:%M:%S")
logging.root.setLevel(logging.INFO)

def pihole_stats ():
    


    now = datetime.datetime.now()
    current_time = now.strftime("%H:%M:%S")

    return_dict={}
    return_dict["last_updated"]=current_time
    return_dict["stats"]={}
    return_dict["stats"]["status"]="Not Available"
    return_dict["stats"]["dns_queries_today"]="Not Available"
    return_dict["stats"]["unique_domains"]="Not Available"
    return_dict["stats"]["ads_blocked_today"]="Not Available"
    
    global config_fname

    current_conf=open_config(fname=config_fname)
    pihole_loc=current_conf.get("pihole",{}).get("piholehost",None)

    if not pihole_loc:
        return_dict["stats"]["status"]="Unconfigured"
        return return_dict

    full_url=f"http://{pihole_loc}/admin/api.php"

    logging.info(f"request pihole stats for {pihole_loc}")

    r = requests.get(full_url)


    if r.status_code == 200:
        pihole_res=json.loads(r.text)

        #extract specific keys from pihole response so keys can stay consistent
        #for the JS front end dealing with the API response
        return_dict["stats"]["status"]=pihole_res["status"]
        return_dict["stats"]["dns_queries_today"]=pihole_res["dns_queries_today"]
        return_dict["stats"]["unique_domains"]=pihole_res["unique_domains"]
        return_dict["stats"]["ads_blocked_today"]=pihole_res["ads_blocked_today"]

    else:
        return_dict["stats"]["status"]="Not Available"
        return_dict["stats"]["dns_queries_today"]=="Not Available"
        return_dict["stats"]["unique_domains"]="Not Available"
        return_dict["stats"]["ads_blocked_today"]="Not Available"

    return return_dict

#print(pihole_stats())