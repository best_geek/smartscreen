"""
Version: 1.5
Author: Nathan Webb
Date Created: 20220120
flask API for sysstatus
"""

import flask
from flask import jsonify
from flask_cors import CORS
from flask import request

import logging
import os
import sys

from endpoints.config.config import *
from endpoints.weather import *
from endpoints.sysstat import *
from endpoints.netcheck import *
from endpoints.greeting import * 
from endpoints.pihole import *

#setup logging
logging.basicConfig(format="[%(filename)s:%(lineno)d] [fun:%(funcName)s] %(asctime)s [%(levelname)s] %(message)s", datefmt="%m/%d/%Y_%I:%M:%S")
logging.root.setLevel(logging.DEBUG)

# configure flask
app = flask.Flask(__name__)
LISTEN_PORT = 5000
CORS(app)  # remove when not doing cross origin

cwd=os.getcwd()
logging.info(f"Currently operating in {cwd}")

# default uri call
@app.route("/", methods=["GET"])
def base():
    status = {"name": "ssapi", "state": "operational", "version": "1.0"}
    return jsonify(status)

#get weather
@app.route("/sysstat", methods=["GET"])
def get_systat():
    result=get_sysstat()
    logging.debug(result)
    return jsonify(result)


#get neteck
@app.route("/netcheck", methods=["GET"])
def get_netcheck():
    result=netcheck()
    logging.debug(result)
    return jsonify(result)

#get weather
@app.route("/weather", methods=["GET"])
def get_weather():
    result=weather()
    logging.debug(result)
    
    response=jsonify(result)
    response.headers.add('Access-Control-Allow-Origin', '*')

    return response

#get greeting
@app.route("/greeting", methods=["GET"])
def get_greeting():
    result=greeting()
    logging.debug(result)

    response=jsonify(result)
    response.headers.add('Access-Control-Allow-Origin', '*')


    return response

#get pihole
@app.route("/pihole", methods=["GET"])
def get_pihole():
    result=pihole_stats()
    logging.debug(result)

    response=jsonify(result)
    response.headers.add('Access-Control-Allow-Origin', '*')


    return response

#get settings
@app.route("/settings/get", methods=["GET"])
def settings_get():
    result=open_config(fname=config_fname)
    logging.debug(result)

    response=jsonify(result)
    response.headers.add('Access-Control-Allow-Origin', '*')


    return response

#update settings
@app.route("/settings/post", methods=["GET"])
def settings_post():
    
    #get sent data
    new_settings = request.headers["updated_settings"]
    new_settings=json.loads(new_settings)

    #update
    if new_settings:
        update_config(data=new_settings,fname=config_fname)
        result={"result":"ok!"}
    else:
        result={"result":"fail","failure_reason":"no settings supplied with request"}

    response=jsonify(result)
    response.headers.add('Access-Control-Allow-Origin', '*')


    return response


#so we can run as a module
if __name__ == "__main__":
    app.run(host="0.0.0.0",debug=False, port=200)
