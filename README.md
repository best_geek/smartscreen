# SmartScreen

# Deploy:
- Clone the repo
- Run `docker-compose build`
- Run `docker-compose up -d`
- Load `https://localhost:8089`
- Profit

# Issues
You can regenerate certificates by running the script in 
`./v2/apache/certs/create-certs.sh`

# Detailed writeup is located at the blog